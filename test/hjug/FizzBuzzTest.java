package hjug;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class FizzBuzzTest {
    
    FizzBuzz instance;

    public FizzBuzzTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new FizzBuzz();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcFizzBuzz method, of class FizzBuzz.
     */
    @Test
    public void testCalcFizzBuzz() {
        System.out.println("calcFizzBuzz");
        int maxNumber = 0;
        instance.generateFizzBuzz(maxNumber);
    }

    @Test
    public void testFizzBuzz() {
        assertEquals("Zero is not 0", "0", instance.getFizzBuzz(0));
        assertEquals("One is not 1", "1", instance.getFizzBuzz(1));
        assertEquals("Two is not 2", "2", instance.getFizzBuzz(2));
        assertEquals("Three is not Fizz", "Fizz", instance.getFizzBuzz(3));
        assertEquals("Four is not 4", "4", instance.getFizzBuzz(4));
        assertEquals("Five is not Buzz", "Buzz", instance.getFizzBuzz(5));
        assertEquals("Six is not Fizz", "Fizz", instance.getFizzBuzz(6));
        assertEquals("Seven is not 7", "7", instance.getFizzBuzz(7));
        assertEquals("Eight is not 8", "8", instance.getFizzBuzz(8));
        assertEquals("Nine is not Fizz", "Fizz", instance.getFizzBuzz(9));
        assertEquals("Ten is not Buzz", "Buzz", instance.getFizzBuzz(10));
        assertEquals("Eleven is not 11", "11", instance.getFizzBuzz(11));
        assertEquals("Twelve is not Fizz", "Fizz", instance.getFizzBuzz(12));
        assertEquals("Thirteen is not 13", "13", instance.getFizzBuzz(13));
        assertEquals("Fourteen is not 14", "14", instance.getFizzBuzz(14));
        assertEquals("Fifteen is not FizzBuzz", "FizzBuzz", instance.getFizzBuzz(15));
    }

    // Fizz Tests
    @Test
    public void testThreeIsFizz() {
        assertEquals("One is not empty", "", instance.getFizz(1));
        assertEquals("Two is not empty", "", instance.getFizz(2));
        assertEquals("Three is not Fizz", "Fizz", instance.getFizz(3));
        assertEquals("Four is not empty", "", instance.getFizz(4));
        assertEquals("Five is not empty", "", instance.getFizz(5));
        assertEquals("Six is not Fizz", "Fizz", instance.getFizz(6));
    }

    // Buzz Tests
    @Test
    public void testFiveIsBuzz() {
        assertEquals("One is not empty", "", instance.getBuzz(1));
        assertEquals("Two is not empty", "", instance.getBuzz(2));
        assertEquals("Three is not Empty", "", instance.getBuzz(3));
        assertEquals("Four is not Empty", "", instance.getBuzz(4));
        assertEquals("Five is not Buzz", "Buzz", instance.getBuzz(5));
        assertEquals("Six is not empty", "", instance.getBuzz(6));
        assertEquals("Seven is not empty", "", instance.getBuzz(7));
        assertEquals("Eight is not Empty", "", instance.getBuzz(8));
        assertEquals("Nine is not Empty", "", instance.getBuzz(9));
        assertEquals("Ten is not Buzz", "Buzz", instance.getBuzz(10));
    }
}

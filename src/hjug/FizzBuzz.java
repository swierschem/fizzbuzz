/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hjug;

import au.com.pegasustech.demos.layout.SCLayout;
import au.com.pegasustech.demos.layout.SGLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.ScrollPane;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class FizzBuzz {
    
    JFrame mainWindow;
    JLabel maxNumberPrompt;
    JButton enterButton;
    JButton cancelButton;
    JTextField maxNumber;
    JTextPane resultList;
    StyledDocument resultDoc;
    Style resultStyle;
    String outputText;

    FizzBuzz() {
        outputText = new String();
        initUI();
        mainWindow.setVisible(true);
    }

    private void initUI() {
        mainWindow = new JFrame("The Ultimate FizzBuzz");
        mainWindow.setPreferredSize(new Dimension(300, 800));
        SCLayout fullBox = new SCLayout(2);
        SGLayout buttons = new SGLayout(2, 2);
        fullBox.setRowScale(1, 10);
        JPanel displayPanel = new JPanel(buttons);

        maxNumberPrompt = new JLabel("How high do we count?");
        maxNumber = new JTextField(10);
        enterButton = new JButton("Enter");
        cancelButton = new JButton("Cancel");
        resultList = new JTextPane();
        resultDoc = resultList.getStyledDocument();
        resultStyle = resultList.addStyle("Result Style", null);
        ScrollPane scroller = new ScrollPane();
        scroller.add(resultList);

        displayPanel.add(maxNumberPrompt);
        displayPanel.add(maxNumber);
        displayPanel.add(enterButton);
        displayPanel.add(cancelButton);
        mainWindow.setLayout(fullBox);
        mainWindow.add(displayPanel);
        mainWindow.add(scroller);
        mainWindow.pack();

        maxNumber.addActionListener((e) -> {
            generateFizzBuzz(Integer.parseInt(maxNumber.getText()));
        });

        enterButton.addActionListener((e) -> {
            generateFizzBuzz(Integer.parseInt(maxNumber.getText()));
        });
        
        cancelButton.addActionListener((e) -> {
            System.exit(0);
        });
    }

    public void generateFizzBuzz(int maxNumber) {
        String crlf = System.getProperty("line.separator");
        try {
            resultDoc.remove(0, resultDoc.getLength());
        } catch (BadLocationException ex) {
            System.out.println("Bad Location Exception" + ex.getMessage());
        }
        for (int i = 1; i <= maxNumber; ++i) {
            StringBuilder sb = new StringBuilder();
            sb.append(getFizzBuzz(i)).append(crlf);
            try {
                resultDoc.insertString(resultDoc.getLength(),
                        sb.toString(), resultStyle);
            } catch (BadLocationException e) {
                System.out.println("Bad Location Exception" + e.getMessage());
            }
        }
    };

    public String getFizzBuzz(int num) {
        if (num == 0) {
            return "0";
        }
        String rc = getFizz(num) + getBuzz(num);
        if (rc.length() == 0) {
            StyleConstants.setForeground(resultStyle, Color.black);
            rc = String.valueOf(num);
        }
        else if (rc.length() > "Fizz".length()) {
            StyleConstants.setForeground(resultStyle, Color.magenta);
        }
        return rc;
    }

    public String getFizz(int num) {
        String rc = "";
        if (num % 3 == 0) {
            StyleConstants.setForeground(resultStyle, Color.red);
            rc = "Fizz";
        }
        return rc;
    }

    public String getBuzz(int num) {
        String rc = "";
        if (num %5 == 0) {
            StyleConstants.setForeground(resultStyle, Color.blue);
            rc = "Buzz";
        }
        return rc;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FizzBuzz fb = new FizzBuzz();

    }
    
}
